import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducers/index';
//warings the mutable of state
import thunk from 'redux-thunk';

export default function confiureStore(initialState) {
	return createStore(rootReducer, initialState, applyMiddleware(thunk));
}
