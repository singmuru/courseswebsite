import React from 'react';
import { Link } from 'react-router-dom';

const HomePage = () => (
	<div className="jumbotron">
		<h1>Courses Home Page </h1>
		<p>we can able view list of courses availiable and add new courses</p>
		<Link to="about" className="btn btn-primary btn-lg">
			About US
		</Link>
	</div>
);

export default HomePage;
