import React from 'react';
import CourseForm from './CourseForm';
import { shallow } from 'enzyme';

function renderCourseForm(args) {
	const defaultProps = {
		course: {},
		authors: [],
		onSave: () => {},
		onChange: () => {},
		saving: false,
		errors: {},
	};

	const props = { ...defaultProps, ...args };
	return shallow(<CourseForm {...props}></CourseForm>);
}

it('render form and header', () => {
	expect(true).toBe(true);
	// const wrapper = renderCourseForm();
	// except(wrapper.find('form').length).toBe(1);
});
