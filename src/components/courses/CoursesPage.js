import React from 'react';
import { connect } from 'react-redux';
import * as courseActions from '../../redux/actions/courseActions';
import * as authorActions from '../../redux/actions/authorActions';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import CourseList from './CourseList';
import { Redirect } from 'react-router-dom';
import Spinner from '../common/Spinner';
import { toast } from 'react-toastify';
class CoursesPage extends React.Component {
	state = {
		redirectToAddCourse: false,
	};
	// constructor(props) {
	// 	super(props);

	// 	this.state = {
	// 		course: {
	// 			title: '',
	// 		},
	// 	};
	// }

	// state = {
	// 	course: {
	// 		title: '',
	// 	},
	// };

	// handleChange = (event) => {
	// 	const course = { ...this.state.course, title: event.target.value };
	// 	this.setState({ course });
	// };

	// handleSumbit = (event) => {
	// 	event.preventDefault();
	// 	// this.props.dispatch(courseActions.createCourse(this.state.course));
	// 	// this.props.createCourse(this.state.course);
	// 	this.props.actions.createCourse(this.state.course);
	// };

	componentDidMount() {
		const { courses, authors, actions } = this.props;

		if (courses.length === 0) {
			actions.loadCourses().catch((error) => {});
		}
		if (authors.length === 0) {
			actions.loadAuthores().catch((error) => {});
		}
	}

	handleDeleteCourse = async (course) => {
		toast.success('Course deleted');
		try {
			await this.props.actions.deleteCourse(course);
		} catch (error) {
			toast.error('Delete failed' + error.message, { autoClose: false });
		}
	};

	render() {
		return (
			// // <form onSubmit={this.handleSumbit}>
			// 	<h2>Courses</h2>
			// 	// <h3>Add Course</h3>
			// 	// <input
			// 	// 	type="text"
			// 	// 	onChange={this.handleChange}
			// 	// 	value={this.state.course.title}
			// 	// />
			// 	// <input type="submit" value="Save"></input>

			// 	{this.props.courses.map((course) => (
			// 		<div key={course.title}>{course.title}</div>
			// 	))}
			// // </form>
			<>
				{this.state.redirectToAddCourse && <Redirect to="/course" />}
				<h2>Courses</h2>

				{this.props.loading ? (
					<Spinner />
				) : (
					<>
						<CourseList
							courses={this.props.courses}
							onDeleteClick={this.handleDeleteCourse}
						></CourseList>
						<button
							style={{ marginBottom: 20 }}
							className="btn btn-primary add-course"
							onClick={() => this.setState({ redirectToAddCourse: true })}
						>
							Add Course
						</button>
					</>
				)}
			</>
		);
	}
}

CoursesPage.propTypes = {
	authors: PropTypes.array.isRequired,
	courses: PropTypes.array.isRequired,
	actions: PropTypes.object.isRequired,
	loading: PropTypes.bool.isRequired,
};

// function mapStateToProps(state, ownProps) {
function mapStateToProps(state) {
	debugger;
	return {
		courses:
			state.authors.length === 0
				? []
				: state.courses.map((course) => {
						return {
							...course,
							authorName: state.authors.find((a) => a.id == course.authorId)
								.name,
						};
						// eslint-disable-next-line no-mixed-spaces-and-tabs
				  }),
		authors: state.authors,
		loading: state.apiCallsInProgress > 0,
	};
}

function mapDispatchToProps(dispath) {
	return {
		// createCourse: (course) => dispath(courseActions.createCourse(course)),
		actions: {
			loadCourses: bindActionCreators(courseActions.loadCourses, dispath),
			loadAuthores: bindActionCreators(authorActions.loadAuthors, dispath),
			deleteCourse: bindActionCreators(courseActions.deleteCourse, dispath),
		},
	};
}

// const connectStateAndProps = connect(mapStateToProps, mapDispatchToProps);
// const connectStateAndProps = ;
export default connect(mapStateToProps, mapDispatchToProps)(CoursesPage);
