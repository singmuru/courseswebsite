import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { loadCourses, saveCourse } from '../../redux/actions/courseActions';
import { loadAuthors } from '../../redux/actions/authorActions';
import CourseForm from './CourseForm';
import { newCourse } from '../../../tools/mockData';
import Spinner from '../common/Spinner';
import { toast } from 'react-toastify';
const ManageCoursePages = ({
	courses,
	authors,
	loadAuthors,
	loadCourses,
	saveCourse,
	history,
	...props
}) => {
	const [course, setCourse] = useState({ ...props.course });
	const [error, setError] = useState({});
	const [isSaveSuccess, setIsSaveSuccess] = useState(false);

	useEffect(() => {
		if (courses.length === 0) {
			loadCourses().catch((error) => {});
		} else {
			setCourse({ ...props.course });
		}
		if (authors.length === 0) {
			loadAuthors().catch((error) => {});
		}
	}, [props.course]);

	const isValidateSaveInfo = () => {
		const { title, authorId, category } = course;
		debugger;
		const errors = {};
		if (!title) errors.title = 'Title is required.';
		if (!authorId) errors.author = 'Author is required.';
		if (!category) errors.category = 'category is required.';
		setError(errors);
		return Object.keys(errors).length === 0;
	};

	const handleSave = (event) => {
		event.preventDefault();
		if (!isValidateSaveInfo()) return;
		setIsSaveSuccess(true);
		saveCourse(course)
			.then(() => {
				toast.success('course saved');
				history.push('/courses');
			})
			.catch((error) => {
				setIsSaveSuccess(false);
				setError({ onSave: error.message });
			});
	};

	const handleChange = (event) => {
		const { name, value } = event.target;

		setCourse((prevCourse) => ({
			...prevCourse,
			[name]: name === 'authorId' ? parseInt(value, 10) : value,
		}));
	};

	return authors.length === 0 || courses.length === 0 ? (
		<Spinner></Spinner>
	) : (
		<CourseForm
			course={course}
			errors={error}
			authors={authors}
			onChange={handleChange}
			onSave={handleSave}
			saving={isSaveSuccess}
		></CourseForm>
	);
};

ManageCoursePages.propTypes = {
	course: PropTypes.object.isRequired,
	authors: PropTypes.array.isRequired,
	courses: PropTypes.array.isRequired,
	loadAuthors: PropTypes.func.isRequired,
	loadCourses: PropTypes.func.isRequired,
	saveCourse: PropTypes.func.isRequired,
	history: PropTypes.object.isRequired,
};

const getCourseBySlug = (courses, slug) => {
	return courses.find((course) => course.slug === slug) || null;
};

//ownProps get props from componet url.
function mapStateToProps(state, ownProps) {
	const slug = ownProps.match.params.slug;
	const course =
		slug && state.courses.length > 0
			? getCourseBySlug(state.courses, slug)
			: newCourse;
	return {
		course,
		courses: state.courses,
		authors: state.authors,
	};
}

const mapDispatchToProps = {
	loadCourses,
	loadAuthors,
	saveCourse,
};

export default connect(mapStateToProps, mapDispatchToProps)(ManageCoursePages);
