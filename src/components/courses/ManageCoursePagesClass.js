import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { loadCourses } from '../../redux/actions/courseActions';
import { loadAuthors } from '../../redux/actions/authorActions';

class ManageCoursePages extends Component {
	constructor(props) {
		super(props);
	}

	componentWillMount() {}

	componentDidMount() {
		const { courses, authors, loadAuthors, loadCourses } = this.props;
		if (courses.length === 0) {
			loadCourses().catch((error) => {
				alert('failed to load courses', error);
			});
		}
		if (authors.length === 0) {
			loadAuthors().catch((error) => {
				alert('failed to load authors', error);
			});
		}
	}

	componentWillReceiveProps(nextProps) {}

	shouldComponentUpdate(nextProps, nextState) {}

	componentWillUpdate(nextProps, nextState) {}

	componentDidUpdate(prevProps, prevState) {}

	componentWillUnmount() {}

	render() {
		return (
			<>
				<h2>Manage Course</h2>
			</>
		);
	}
}

ManageCoursePages.propTypes = {
	authors: PropTypes.array.isRequired,
	courses: PropTypes.array.isRequired,
	loadAuthors: PropTypes.func.isRequired,
	loadCourses: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
	return {
		courses: state.courses,
		authors: state.authors,
	};
}

const mapDispatchToProps = {
	loadCourses,
	loadAuthors,
};

export default connect(mapStateToProps, mapDispatchToProps)(ManageCoursePages);
